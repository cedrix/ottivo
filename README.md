# Ottivo 

## Coding Challenge

### Installation:
The code requires a database to be configured in `.env`-File.

Let composer run on a fresh system:
`composer install`

Create an empty database and check configuration:
`./bin/console doctrine:database:create`

Set up database table:
`./bin/console doctrine:migrations:migrate`

Fill the database:
`./bin/console doctrine:fixtures:load`

### Main:
The main command can be run with:
`./bin/console app:showVacation <year> [filename]`

Year (required) specifies the year for the calculation of the vacation-days.

Filename is optional to save the result. If this is omitted the result will be printed to screen.
The format of the file is JSON. 

## Domain Assumptions:
- Special-contracts override the automatic addition of vacation-days related to age etc.
- Special-contracts in the starting year get treated proportionally.
- When a person has not been an employee in the given year the result of vacation days is set to zero.

## Implementations:
- The logic of rules #1-#4 have been put in a separate testable class. 
- Each rule has its own function, when appropriate.
- The rule 5 is implemented with a constraint. In case the starting date is neither 1st or 15th of a month then a domain-exception will be thrown.

## Tests:
- The calculations can be tested via PHPUnit-Tests `./bin/phpunit`

### Enjoy!

### Lars
