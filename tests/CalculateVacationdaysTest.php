<?php

namespace App\Tests;

use App\Helper\CalculateVacationDays;
use PHPUnit\Framework\TestCase;

class CalculateVacationdaysTest extends TestCase
{
    /**
     * 26 Days on default
     *
     * @throws \Exception
     */
    public function testCalculateVacationDaysRuleOne()
    {
        $calculator = new CalculateVacationDays();
        $vacationDays = $calculator->calculateVacationDays(
            new \DateTime('2015-01-01'),
            new \DateTime('2018-01-01'),
            new \DateTime('2019-01-01')
        );
        $this->assertEquals(26, $vacationDays);
    }

    /**
     * When Employee turns 30 he gets +1 day, consecutively every 5 years from then.
     *
     * @throws \Exception
     */
    public function testCalculateVacationDaysRuleTwo()
    {
        $calculator = new CalculateVacationDays();
        $vacationDays = $calculator->calculateVacationDays(
            new \DateTime('1970-01-01'),
            new \DateTime('2010-01-01'),
            new \DateTime('2023-01-01')
        );
        $this->assertEquals(26+5, $vacationDays);
    }

    /**
     * Special contracts overrides with priority and always valid.
     *
     * @throws \Exception
     */
    public function testCalculateVacationDaysRuleThree()
    {
        $calculator = new CalculateVacationDays();
        $vacationDays = $calculator->calculateVacationDays(
            new \DateTime('1970-01-01'),
            new \DateTime('2010-01-01'),
            new \DateTime('2023-01-01'),
            100
        );
        $this->assertEquals(100, $vacationDays);
    }

    /**
     * Fraction of a year will cut the first month and count the
     * months left of the year proportionally.
     *
     * @throws \Exception
     */
    public function testCalculateVacationDaysRuleFour()
    {
        $calculator = new CalculateVacationDays();
        $vacationDays = $calculator->calculateVacationDays(
            new \DateTime('2000-01-01'),
            new \DateTime('2001-06-01'),
            new \DateTime('2001-01-01')
        );
        $this->assertEquals(13, $vacationDays);
    }

}
