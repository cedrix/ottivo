<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VacationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Vacation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $dateOfBirth;

    /**
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $specialVacationDays;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateOfBirth(): \DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getSpecialVacationDays(): ?int
    {
        return $this->specialVacationDays;
    }

    public function setSpecialVacationDays(?int $specialVacationDays): self
    {
        $this->specialVacationDays = $specialVacationDays;

        return $this;
    }

    /**
     * Apply Rule #5 with Constraints / Callback
     *
     * @PrePersist
     * @PreUpdate
     * @throws \Exception
     */
    public function assertEmployeeStartsOnSpecificDates(): void
    {
        if ($this->checkStartingDateIsFirstOrFifteenth($this->getStartDate()->format('d'))) {
            throw new \DomainException('Employees must start on 1st or 15th of a month.');
        }
    }

    private function checkStartingDateIsFirstOrFifteenth(string $day): bool
    {
        return ($day!=='01') && ($day!=='15');
    }
}
