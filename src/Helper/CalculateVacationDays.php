<?php

namespace App\Helper;

class CalculateVacationDays
{
    /**
     * Calculates the vacationdays for a given year, based on Rules 1-4)
     *
     * @param \DateTimeInterface $dateOfBirth
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $calcYear
     * @param int|null $specialVacationDays
     * @return int
     */
    public function calculateVacationDays(
        \DateTimeInterface $dateOfBirth,
        \DateTimeInterface $startDate,
        \DateTimeInterface $calcYear,
        int $specialVacationDays = null
    ): int {
        // Vacation only for Employees
        if ($startDate->format('Y')>$calcYear->format('Y')) {
            return 0;
        }

        // Rule #1 (default)
        $vacationDays = 26;

        // Rule #2 additional day for every 5 years equal or above 30.
        $age = $dateOfBirth->diff($calcYear);
        $vacationDays += $age->invert ? 0 : $this->additionalDayEveryFiveYears($age->y);

        // Rule #3 SpecialContract overrides.
        $vacationDays = $specialVacationDays ?? $vacationDays;

        // Rule #4 Check if Start-Year is specific year
        if ($startDate->format('Y')===$calcYear->format('Y')) {
            $vacationDays = $this->inTheCourseOfTheYear($startDate, $vacationDays);
        }

        // Rule #5 It does not matter if a person starts on 1st or 15th. Gets checked by Validator ;)

        return (int)$vacationDays;
    }

    /**
     * 29 = 0 days
     * 30 = 1 day ...
     * 34 = 1 day
     * 35 = 2 days ...
     * 39 = 2 days
     * 40 = 3 days ...
     *
     * @param $age
     * @return int
     */
    public function additionalDayEveryFiveYears(int $age): int
    {
        return (int)max(0, ceil(($age-29) / 5));
    }

    /**
     * Returns rounded 1/12 of vacationdays for each remaining month in the year, beginning on 1st of February.
     *
     * @param $startDate
     * @param $vacationDays
     * @return int
     */
    public function inTheCourseOfTheYear(\DateTimeInterface $startDate, int $vacationDays): int
    {
        return (int)round($vacationDays * (12 - $startDate->format('n')) / 12);
    }
}
