<?php

namespace App\DataFixtures;

use App\Entity\Vacation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class VacationFixtures extends Fixture
{
    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $employees = new Vacation();
        $employees->setName('Hans Müller')
            ->setDateOfBirth(new \DateTime('1950-12-30'))
            ->setStartDate(new \DateTime('2001-01-01'));
        $manager->persist($employees);

        $employees = new Vacation();
        $employees->setName('Angelika Fringe')
            ->setDateOfBirth(new \DateTime('1966-06-09'))
            ->setStartDate(new \DateTime('2001-01-15'));
        $manager->persist($employees);

        $employees = new Vacation();
        $employees->setName('Peter Klever')
            ->setDateOfBirth(new \DateTime('1991-07-12'))
            ->setStartDate(new \DateTime('2016-05-15'))
            ->setSpecialVacationDays(27);
        $manager->persist($employees);

        $employees = new Vacation();
        $employees->setName('Marina Helter')
            ->setDateOfBirth(new \DateTime('1970-01-26'))
            ->setStartDate(new \DateTime('2018-01-15'));
        $manager->persist($employees);

        $employees = new Vacation();
        $employees->setName('Sepp Meier')
            ->setDateOfBirth(new \DateTime('1980-05-25'))
            ->setStartDate(new \DateTime('2017-12-01'));
        $manager->persist($employees);

        $manager->flush();
    }
}
