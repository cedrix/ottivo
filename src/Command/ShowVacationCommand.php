<?php

namespace App\Command;

use App\Helper\CalculateVacationDays;
use App\Repository\VacationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ShowVacationCommand extends Command
{
    protected static $defaultName = 'app:showVacation';

    private $vacationRepository;
    private $calculateVacationDays;

    public function __construct(VacationRepository $vacationRepository, CalculateVacationDays $calculateVacationDays)
    {
        parent::__construct();
        $this->vacationRepository = $vacationRepository;
        $this->calculateVacationDays = $calculateVacationDays;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Saves the number of vacation-days for a specific year.')
            ->addArgument('year', InputArgument::REQUIRED, 'Which Year')
            ->addArgument('filename', InputArgument::OPTIONAL, 'Filename or STDOUT')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $year = $input->getArgument('year');
        $filename = $input->getArgument('filename');

        if ($year) {
            $io->note(sprintf('Getting vacations for year: %s', $year));
        }

        if ($filename) {
            $io->note(sprintf('Saving result to: %s', $filename));
        }

        try {
            $yearDate = new \DateTime("31-12-$year");
        } catch (\Exception $e) {
            $io->error(sprintf('This "%s" looks not like a year to me.', $year));
            return;
        }

        $result=[];

        foreach ($this->vacationRepository->findAll() as $employee) {
            $name = $employee->getName();
            $dateOfBirth = $employee->getDateOfBirth();
            $startDate = $employee->getStartDate();
            $specialVacationDays = $employee->getSpecialVacationDays();
            $vacationDays = $this->calculateVacationDays->calculateVacationDays($dateOfBirth, $startDate, $yearDate, $specialVacationDays);
            $result[$name]=(string)$vacationDays;
        }

        if (!$filename) {
            print_r($result);
        } else {
            file_put_contents($filename, json_encode($result));
        }

        $io->success('Success: Vacation-days for a specific year has been successfully finished. Pass --help to see your options.');
    }
}
